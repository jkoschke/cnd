var _news;
_news = {

    loadToEditor: function (newsID) {
        $.get('/api/news/single/' + newsID, function (data) {
            if (data.result == 'success') {
                editor.setHTML(data.news.text);
                $('input[name=newsTitle]').val(data.news.title);
                $('#hiddenNewsID').val(data.news.newsID);
                $.get('/api/category/all/', function (categorydata) {
                    var _combobox = $('#newsEditModal').find('.selectCategory select');
                    _combobox.find('option').remove().end();
                    $.each(categorydata.newscategories, function (i, item) {
                        var _option = $('<option></option>').val(item.categoryID).html(item.name);
                        if (item.categoryID == data.news.category.newsCategoryID) {
                            _option.attr('selected', true);
                        }
                        _combobox.append(_option);
                        $('#newsEditModal').modal('show');
                    });
                });
            }
        });
    },
    update: function (newsID) {
        var _titel = $('input[name=newsTitle]').val();
        var _text = editor.getHTML();
        var _category = $('#newsEditModal').find('.selectCategory select > :selected').val();
        $.post('/api/news/single/' + newsID + '/update', {
            title: _titel,
            text: _text,
            categoryid: _category
        }, function(data){
            if(data.result == 'success'){
                var _newsitem = $('#newsItem'+newsID);
                _newsitem.replaceWith($(data.render));
                _newsitem.find('#newsBody'+newsID).collapse('show');
                $('#newsEditModal').modal('hide');
            }
        });
    },
    getCategories: function (result) {
        $.get('/api/category/all/', function (categorydata) {
            result = categorydata;
            return result;
        });
    },
    setItemEditButtonHandler: function () {
        $('.editNews').click(function () {
            _news.loadToEditor($(this).data('newsid'));
        });
    },
    setEditSaveButtonHandler: function () {
        $('#newsEditModal').find('.btn-save').click(function () {
            _news.update($('#hiddenNewsID').val());
        });
    }
};

_news.setItemEditButtonHandler();
_news.setEditSaveButtonHandler();