﻿# Chor News and Download Page (CND)

## Was ist CND?

CND ist ein kleines CMS gedacht zur Verwaltung von News und div. Downloads eines Vereins oder im expliziten Fall eines Chors.

Es ist ein in php geschriebens System mit Verwendung des Frameworks [PHPixie](http://phpixie.com)

## Was kann CND?

Wie der Name schon sagt, ermöglicht es eine Verwaltung von News und Downloads.
Der Grundgedanke liegt darin, dass es eine, nur für vereinszugehörige Personen zugängliche Plattform ist.

CND unterstützt somit:

* Verwaltung meherer Benutzer

    Es gibt ein Rechte- und Rollensystem, womit die möglichen Funktionen freigegeben bzw. gesperrt werden können.

* Verwaltung von News

    User mit entsprechenden Rechten können News erstellen, bearbeiten und löschen.

* Kommentieren

    Es gibt für User die Möglichkeit, Kommentare zu Newsbeiträgen zu hinterlassen.

* Verwaltung von Downloads

    Eine einfache Verwaltung von Dateien, die für Nutzer bereit gestellt werden sollen.
    Hierbei können einzelne Datein zu Downloadgruppen zusammengefasst werden.
    - Beispiel: Lieder
        - Songtext
        - Audiodatei
        - Noten
        - Sonstiges

copyright by J. Koschke