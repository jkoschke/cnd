<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 06.08.2015
 * Time: 08:34
 */

namespace App\Controller\Api\Auth;


use App\Controller\Api;
use App\Controller\Api\AbstractApiMethod;
use App\Exception\AccessDeniedException;
use App\Model\ApiResponse;

class Login extends AbstractApiMethod
{

    /**
     * @var string
     */
    private $NEEDED_RIGHT = 'user.login';


    /**
     * @return ApiResponse
     * @throws AccessDeniedException
     */
    public function action_post()
    {
        $login = strtolower($this->request->post('email'));
        $password = $this->request->post('password');


        /**
         * @var \App\Model\Account $account
         */
        $account = $this->pixie->orm->get('account')->where('email', $login)->find();
        if (!$account->hasRight($this->NEEDED_RIGHT)) {
            throw new AccessDeniedException("You don't have the right to do this action!");
        }


        $logged = $this->pixie->auth
            ->provider('password')
            ->login($login, $password);


        if (!$logged)
            return new ApiResponse(array('message' => 'Login failed'));

        return new ApiResponse(array());
    }


}