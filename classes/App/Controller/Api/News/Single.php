<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 06.08.2015
 * Time: 09:53
 */

namespace App\Controller\Api\News;


use App\Controller\Api\AbstractApiMethod;
use App\Exception\AccessDeniedException;
use App\Exception\ApiException;
use App\Model\ApiResponse;

class Single extends AbstractApiMethod
{

    /**
     * @return mixed
     * @throws ApiException
     */
    public function action_post()
    {
        $action = $this->request->param('subaction');
        $method = 'post_' . strtolower($action);

        if (method_exists($this, $method)) {
            return $this->$method();
        } else {
            throw new ApiException('Method ' . $method . ' not found!', 404);
        }
    }

    /**
     * @return ApiResponse
     * @throws AccessDeniedException
     */
    private function post_update()
    {
        $needed_right = 'news.edit';

        /**
         * @var \App\Model\Account $user
         */
        $user = $this->pixie->auth->user();
        if ($user->hasRight($needed_right) === false) {
            throw new AccessDeniedException('You don\'t have the right to do this!');
        }

        $newsID = $this->request->param('id');

        $title = $this->request->post('title');
        $text = $this->request->post('text', '', false);
        $category = $this->request->post('categoryid');


        /**
         * @var \App\Model\News $news
         */
        $news = $this->pixie->orm->get('news')->where('newsID', $newsID)->find();
        if ($news->loaded()) {
            if ($this->pixie->orm->get('newscategory')->where('newsCategoryID', $category)->find()->loaded()) {
                $news->title = $title;
                $news->text = $text;
                $news->category = $category;
                $news->updater = $user->accountID;
                $news->updated_at = date('Y-m-d H:i:s');
                $news->save();
            }

            $newsRender = $this->pixie->haml->get('partial/newsitem');
            $newsRender->user = $user;
            $newsRender->newsitem = $news;
            return new ApiResponse(array('render' => trim($newsRender->render())));
        }
    }

    /**
     * @return ApiResponse
     * @throws AccessDeniedException
     */
    private function post_create()
    {
        $needed_right = 'news.create';

        /**
         * @var \App\Model\Account $user
         */
        $user = $this->pixie->auth->user();
        if ($user->hasRight($needed_right) === false) {
            throw new AccessDeniedException('You don\'t have the right to do this!');
        }

        $newsID = $this->request->param('id');

        $title = $this->request->post('title');
        $text = $this->request->post('text', '', false);
        $category = $this->request->post('categoryid');


        /**
         * @var \App\Model\News $news
         */
        $news = $this->pixie->orm->get('news')->where('newsID', $newsID);
        if ($this->pixie->orm->get('newscategory')->where('newsCategoryID', $category)->find()->loaded()) {
            $news->title = $title;
            $news->text = $text;
            $news->category = $category;
            $news->updater = $user->accountID;
            $news->updated_at = date('Y-m-d H:i:s');
            $news->save();
        }

        $newsRender = $this->pixie->haml->get('partial/newsitem');
        $newsRender->user = $user;
        $newsRender->newsitem = $news;
        return new ApiResponse(array('render' => trim($newsRender->render())));

    }

    /**
     * @return ApiResponse
     * @throws AccessDeniedException
     * @throws ApiException
     */
    public function action_get()
    {
        $needed_right = 'news.view';

        if (!$this->pixie->auth->user()->hasRight($needed_right))
            throw new AccessDeniedException('You have no rights to do this!');

        /**
         * @var int $newsID
         */
        $newsID = $this->request->param('id');

        /**
         * @var \App\Model\News $news
         */
        $news = $this->pixie->orm->get('news')->where('newsID', $newsID)->find();
        if ($news->loaded()) {
            return new ApiResponse(array('news' => array(
                'newsID' => $newsID,
                'title' => $news->title,
                'text' => $news->text,
                'category' => array(
                    'newsCategoryID' => $news->category,
                    'name' => $news->getCategory()->name
                ),
                'author' => $news->author,
                'created_at' => $news->created_at,
                'updater' => $news->updater,
                'updated_at' => $news->updated_at,
                'archived' => $news->archived
            )));
        } else {
            throw new ApiException('News does not exist', 404);
        }
    }

    /**
     * @return ApiResponse
     * @throws ApiException
     */
    public function action_delete()
    {
        $action = $this->request->param('subaction');
        $method = 'delete_' . strtolower($action);

        if (method_exists($this, $method)) {
            return $this->$method();
        } else {
            throw new ApiException('Method ' . $method . ' not found!', 404);
        }
    }

    /**
     * @return ApiResponse
     * @throws AccessDeniedException
     * @throws ApiException
     */
    private function delete_full()
    {
        $needed_right = 'news.delete';
        /**
         * @var \App\Model\Account $user
         */
        $user = $this->pixie->auth->user();
        if (!$user->hasRight($needed_right)) {
            throw new AccessDeniedException('You have no rights to do this!');
        }

        $newsID = $this->request->param('id');
        $news = $this->pixie->orm->get('news')->where('newsID', $newsID)->find();
        if ($news->loaded()) {
            $comments = $this->pixie->orm->get('comment')->where('newsID', $news->newsID)->find_all();
            $comments->delete_all();
            $news->delete();
            return new ApiResponse(array());
        }
        throw new ApiException("News doesn't exist", 404);
    }

    /**
     * @return ApiResponse
     * @throws AccessDeniedException
     * @throws ApiException
     */
    private function delete_archive()
    {
        $needed_right = 'news.delete';
        /**
         * @var \App\Model\Account $user
         */
        $user = $this->pixie->auth->user();
        if (!$user->hasRight($needed_right)) {
            throw new AccessDeniedException('You have no rights to do this!');
        }

        $newsID = $this->request->param('id');
        $news = $this->pixie->orm->get('news')->where('newsID', $newsID)->find();
        if ($news->loaded()) {
            $news->archive = 1;
            $news->save();
            return new ApiResponse(array());
        }
        throw new ApiException("News doesn't exist", 404);
    }
}