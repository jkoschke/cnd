<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 06.08.2015
 * Time: 14:20
 */

namespace App\Controller\Api\News;


use App\Controller\Api\AbstractApiMethod;
use App\Exception\AccessDeniedException;
use App\Exception\ApiException;
use App\Model\ApiResponse;

class Comment extends AbstractApiMethod
{
    /**
     * @return ApiResponse
     * @throws ApiException
     */
    public function action_get()
    {
        /**
         * @var int $newsID
         */
        $newsID = $this->request->param('id', null);
        if ($newsID === null) {
            throw new ApiException('No NewsID was given', 400);
        }

        /**
         * @var \App\Model\Comment[] $items
         */
        $items = $this->pixie->orm->get('comment')->where('newsID', $newsID)->find_all();
        $commentItems = array();
        foreach ($items as $item) {
            $commentItems[] = $item->as_array();
        }
        return new ApiResponse(array('commentlist' => $commentItems));
    }


    /**
     * @return ApiResponse
     * @throws AccessDeniedException
     * @throws ApiException
     */
    public function action_post()
    {
        $needed_right = 'news.comment.create';

        /**
         * @var \App\Model\Account $user
         */
        $user = $this->pixie->auth->user();
        if ($user->hasRight($needed_right) === false) {
            throw new AccessDeniedException('You don\'t have the right to do this!');
        }

        /**
         * @var int $newsID
         */
        $newsID = $this->request->param('id', null);
        if ($newsID === null) {
            throw new ApiException('No NewsID was given', 400);
        }

        $text = $this->request->post('text');


        /**
         * \App\Model\Comment $comment
         */
        $comment = $this->pixie->orm->get('comment');
        $comment->author = $user->accountID;
        $comment->newsID = $newsID;
        $comment->text = $text;
        $comment->save();

        $render = $this->pixie->haml->get('partial/comment');
        $render->comment = $comment;

        return new ApiResponse(array('render' => trim($render->render())));
    }


}