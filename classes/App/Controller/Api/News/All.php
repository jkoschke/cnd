<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 06.08.2015
 * Time: 10:33
 */

namespace App\Controller\Api\News;


use App\Controller\Api\AbstractApiMethod;
use App\Model\ApiResponse;

class All extends AbstractApiMethod
{

    public function action_get()
    {
        /**
         * @var int $fromID
         */
        $fromID = $this->request->get('begin', 1);


        /**
         * @var int $count
         */
        $count = $this->request->get('count', 10);

        /**
         * @var \App\Model\News
         */
        $items = $this->pixie->orm->get('news')->where('newsID', '>=', $fromID)->limit($count)->find_all();
        $newsItems = array();
        foreach($items as $item){
            $newsItems[] = array(
                'newsID' => $item->newsID,
                'title' => $item->title,
                'text' => $item->text,
                'category' => array(
                    'newsCategoryID' => $item->category,
                    'name' => $item->getCategory()->name
                ),
                'author' => $item->author,
                'created_at' => $item->created_at,
                'updater' => $item->updater,
                'updated_at' => $item->updated_at,
                'archived' => $item->archived
            );
        }
        return new ApiResponse(array('newslist' => $newsItems));

    }

}