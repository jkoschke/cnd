<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 06.08.2015
 * Time: 08:36
 */

namespace App\Controller\Api;


abstract class AbstractApiMethod
{
    /**
     * @var \App\Pixie
     */
    protected $pixie;

    /**
     * @var \PHPixie\Request
     */
    protected $request;

    /**
     * @var \PHPixie\Response
     */
    protected $response;

    /**
     * @param \App\Pixie $pixie
     * @param \PHPixie\Request $request
     */
    public function __construct($pixie, $request)
    {
        $this->pixie = $pixie;
        $this->response = $pixie->response();
        $this->request = $request;
    }

}