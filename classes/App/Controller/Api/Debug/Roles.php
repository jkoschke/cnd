<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 06.08.2015
 * Time: 11:51
 */

namespace App\Controller\Api\Debug;


use App\Controller\Api\AbstractApiMethod;
use App\Exception\ApiException;
use App\Model\ApiResponse;

class Roles extends AbstractApiMethod
{

    /**
     * @return ApiResponse
     */
    public function action_get()
    {
        /**
         * @var \App\Model\Role[] $rights
         */
        //$rights = $this->pixie->orm->get('role')->find_all();
        $roles = $this->pixie->auth->user()->getUserRoles();
        $result = array();
        foreach ($roles as $role) {
            $result[] = array(
                'roleID' => $role->roleID,
                'identifier' => $role->identifier,
                'roleName' => $role->roleName
            );
        }
        return new ApiResponse(array('result' => $result));
    }
}