<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 06.08.2015
 * Time: 11:47
 */

namespace App\Controller\Api\Debug;


use App\Controller\Api\AbstractApiMethod;
use App\Exception\ApiException;
use App\Model\ApiResponse;

class Rights extends AbstractApiMethod
{

    /**
     * @return ApiResponse
     */
    public function action_get()
    {
        /**
         * @var \App\Model\Right[] $rights
         */
        $rights = $this->pixie->orm->get('right')->find_all();
        $result = array();

        foreach ($rights as $right) {
            $result[] = array(
                'rightID' => $right->rightID,
                'identifier' => $right->identifier,
                'rightName' => $right->rightName
            );
        }
        return new ApiResponse(array('result' => $result));
    }

}