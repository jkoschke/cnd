<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 06.08.2015
 * Time: 12:00
 */

namespace App\Controller\Api\Debug;


use App\Controller\Api\AbstractApiMethod;
use App\Exception\ApiException;
use App\Model\ApiResponse;

class Role extends AbstractApiMethod
{

    /**
     * @return ApiResponse
     * @throws ApiException
     */
    public function action_get()
    {
        $id = $this->request->param('id');
        if (is_numeric($id)) {
            $col = 'roleID';
        } else {
            $col = 'identifier';
        }
        /**
         * @var \App\Model\Role $role
         */
        $role = $this->pixie->orm->get('role')->where($col, $id)->find();

        if (!$role->loaded())
            throw new ApiException("Role '{$id}' not found!", 404);


        return new ApiResponse(array(
            'rightID' => $role->roleID,
            'identifier' => $role->identifier,
            'rightName' => $role->roleName));
    }


}