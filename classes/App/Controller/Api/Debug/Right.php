<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 06.08.2015
 * Time: 11:54
 */

namespace App\Controller\Api\Debug;


use App\Controller\Api\AbstractApiMethod;
use App\Exception\ApiException;
use App\Model\ApiResponse;

class Right extends AbstractApiMethod
{

    /**
     * @return ApiResponse
     * @throws ApiException
     */
    public function action_get()
    {
        $id = $this->request->param('id');
        if (is_numeric($id)) {
            $col = 'rightID';
        } else {
            $col = 'identifier';
        }
        /**
         * @var \App\Model\Right $right
         */
        $right = $this->pixie->orm->get('right')->where($col, $id)->find();
        if (!$right->loaded())
            throw new ApiException("Right '{$id}' not found!", 404);


        return new ApiResponse(array(
            'rightID' => $right->rightID,
            'identifier' => $right->identifier,
            'rightName' => $right->rightName));
    }

}