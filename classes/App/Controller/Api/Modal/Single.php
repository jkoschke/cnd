<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 07.08.2015
 * Time: 15:21
 */

namespace App\Controller\Api\Modal;


use App\Controller\Api;
use App\Controller\Api\AbstractApiMethod;
use App\Exception\ApiException;
use App\Model\ApiResponse;

class Single extends AbstractApiMethod
{

    public function action_get()
    {
        $name = $this->request->param('id', null);
        if ($name === null)
            throw new ApiException("Parameter {name} is missing", 406);

        $name = strtolower($name) . "modal";
        $fileExtension = '.haml';

        $pathToModalFolder = __DIR__ . "/../../../../../assets/views/modal/";

        $dir = opendir($pathToModalFolder);
        $apiResponse = null;
        while (($file = readdir($dir)) !== false) {
            if (strtolower($file) === $name.$fileExtension) {
                /**
                 * @var \PHPixie\Haml\View $render
                 */
                $render = $this->pixie->haml->get('Modal/' . $name);

                $apiResponse = new ApiResponse(array('render' => $render->render()));
                break;
            }

        }
        closedir($dir);

        if ($apiResponse === null)
            throw new ApiException('Modal not found', 404);

        return $apiResponse;
    }

}