<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 06.08.2015
 * Time: 11:54
 */

namespace App\Controller\Api\Category;

use App\Controller\Api\AbstractApiMethod;
use App\Exception\ApiException;
use App\Model\ApiResponse;

/**
 * Created by PhpStorm.
 * User: aw14353
 * Date: 06.08.2015
 * Time: 09:19
 */
class All extends AbstractApiMethod
{

    public function action_get()
    {
        $categories = $this->pixie->orm->get('newscategory')->order_by('name', 'asc')->find_all();

        $categorylist = array();
        foreach ($categories as $category) {
            $categorylist[] = array(
                'categoryID' => $category->newsCategoryID,
                'name' => $category->name
            );
        }
        return new ApiResponse(array(
            'newscategories' => $categorylist
        ));
    }


}