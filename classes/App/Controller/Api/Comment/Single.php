<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 06.08.2015
 * Time: 14:10
 */

namespace App\Controller\Api\Comment;


use App\Controller\Api\AbstractApiMethod;
use App\Exception\AccessDeniedException;
use App\Exception\ApiException;
use App\Model\ApiResponse;

class Single extends AbstractApiMethod
{

    /**
     * @return ApiResponse
     * @throws ApiException
     */
    public function action_get()
    {
        /**
         * @var int $commentID
         */
        $commentID = $this->request->param('id', null);
        if ($commentID === null) {
            throw new ApiException('No CommentID was given', 400);
        }

        /**
         * @var \App\Model\Comment $item
         */
        $item = $this->pixie->orm->get('comment')->where('newsCommentID', $commentID)->find();

        return new ApiResponse(array('comment' => $item->as_array()));
    }

    /**
     * Löschen wenn User news.comment.fdelete ODER news.comment.delete & Autor besitzt
     * @return ApiResponse
     * @throws AccessDeniedException
     * @throws ApiException
     * @throws \Exception
     */
    public function action_delete()
    {

        /**
         * @var int $commentID
         */
        $commentID = $this->request->param('id', null);
        if ($commentID === null) {
            throw new ApiException('No CommentID was given', 400);
        }

        $needed_rights = array('news.comment.fdelete', 'news.comment.delete');

        /**
         * @var \App\Model\Comment $comment
         */
        $comment = $this->pixie->orm->get('comment')->where('newsCommentID', $commentID)->find();

        if (!$comment->loaded())
            throw new ApiException('Comment not Found', 404);


        /**
         * @var \App\Model\Account $user
         */
        $user = $this->pixie->auth->user();
        if (!$user->hasRight($needed_rights[0]) && (!$user->hasRight($needed_rights[1]) || $user->accountID !== $comment->author))
            throw new AccessDeniedException('You don\'t have the right to do this!');


        $comment->delete();

        return new ApiResponse(array('message' => $commentID . ' deleted'));
    }


}