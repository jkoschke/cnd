<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 06.08.2015
 * Time: 10:33
 */

namespace App\Controller\Api\Comment;


use App\Controller\Api\AbstractApiMethod;
use App\Exception\AccessDeniedException;
use App\Exception\ApiException;
use App\Model\ApiResponse;

class All extends AbstractApiMethod
{

    /**
     * @return ApiResponse
     * @throws ApiException
     */
    public function action_get()
    {
        /**
         * @var int $newsID
         */
        $newsID = $this->request->param('id', null);
        if ($newsID === null) {
            throw new ApiException('No NewsID was given', 400);
        }

        /**
         * @var int $count
         */
        $count = $this->request->get('count', 10);

        /**
         * @var \App\Model\Comment[] $items
         */
        $items = $this->pixie->orm->get('comment')->where('newsID', $newsID)->limit($count)->find_all();
        $commentItems = array();
        foreach ($items as $item) {
            $commentItems[] = $item->as_array();
        }
        return new ApiResponse(array('commentlist' => $commentItems));
    }

    /**
     * Löschen wenn User news.comment.fdelete besitzt
     * @return ApiResponse
     * @throws AccessDeniedException
     * @throws ApiException
     */
    public function action_delete()
    {
        $needed_right = 'news.comment.fdelete';
        /**
         * @var \App\Model\Account $user
         */
        $user = $this->pixie->auth->user();
        if (!$user->hasRight($needed_right))
            throw new AccessDeniedException('You don\'t have the right to do this!');

        $newsID = $this->request->param('id', null);
        if ($newsID === null) {
            throw new ApiException('No NewsID was given', 400);
        }

        $this->pixie->orm->get('comment')->where('newsID', $newsID)->find_all()->delete_all();
        return new ApiResponse(array('message' => 'Comments deleted'));
    }

}