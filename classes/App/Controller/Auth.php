<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 28.03.2015
 * Time: 19:46
 */

namespace App\Controller;


use App\Exception\AccountNotFoundException;
use PHPixie\Exception\PageNotFound;

class Auth extends \App\AuthPage
{
    public function before()
    {
        parent::before();
        $this->view->nav = false;
    }


    /**
     * @deprecated
     */
    public function action_login()
    {
        $redirectURL = $this->request->get('redirect', '/news/list/');
        if ($this->request->method == 'POST') {
            $login = strtolower($this->request->post('email'));
            $password = $this->request->post('password');

            $logged = $this->pixie->auth
                ->provider('password')
                ->login($login, $password);

            if ($logged)
                $this->redirect($redirectURL);
        }
        $this->setSubview('login');
        $this->view->postURL = '/auth/login/?redirect=' . $redirectURL;
    }

    /**
     * @throws PageNotFound
     */
    public function action_passwordreset()
    {
        if ($this->pixie->auth->user() !== null) {
            $this->redirect('/user/show');
        }
        $method = strtolower($this->request->method) . '_passwordreset';
        if (method_exists($this, $method)) {
            $this->$method();
        } else {
            throw new PageNotFound();
        }
    }

    /**
     * @throws AccountNotFoundException
     */
    private function post_passwordreset()
    {
        $email = $this->request->post('email', null);
        if ($email !== null) {
            $email = strtolower($email);

            /**
             * @var \App\Model\Account $account
             */
            //Get Account by Email
            $account = $this->pixie->orm->get('account')->where('email', $email)->find();

            if (!$account->loaded())
                throw new AccountNotFoundException('Der Account konnte nicht gefunden werden!');

            $this->invalidateOldResetToken($account->accountID);
            $resetToken = $this->generateResetToken($account->accountID)->save();


            //Send Reset-Email
            $this->sendResetTokenEmail($account, "$resetToken->token");


            $this->setSubview('passwordreset');
            $this->view->showMessage = true;
            $this->view->useremail = $email;
        } else {
            /**
             * @var \PHPixie\Validate\Validator $POSTvalidator
             */
            $POSTvalidator = $this->pixie->validate->get($this->request->post());
            $POSTvalidator->field('password')->rule('filled')->rule('min_length', 8);
            $POSTvalidator->field('password_confirm')->rule('filled')->rule('same_as', 'password');

            $token = $this->request->get('token', null);
            $email = $this->request->get('email', null);

            $this->setSubview("passwordreset2");

            if ($token !== null && $email !== null) {
                if (($account = $this->getAccount($email))) {

                    $pwTokenEntry = $this->pixie->orm->get('pwresettoken')->where('token', $token)->where('and', array('accountID', $account->accountID))->find();
                    if ($pwTokenEntry->loaded() && $pwTokenEntry->valid == 1) {

                        if ($POSTvalidator->valid()) {
                            Auth::updatePassword($this->request->post('password'), $this->pixie, $account->accountID);

                            $this->view->message = "Kennwort erfolgreich geändert!";
                            $this->invalidateOldResetToken($account->accountID);
                            $this->view->query = "";
                            return;
                        }
                    }
                    $this->view->message = "Ihr Reset-Token ist nicht mehr gültig.<br>Bitte fordern Sie <a href='/auth/passwordreset'>hier</a> einen neuen an!";
                    $this->view->query = "";
                    return;
                }
            }
            $this->redirect('/login');
        }

    }

    /**
     *
     */
    private function get_passwordreset()
    {
        $token = $this->request->get('token', null);
        $email = $this->request->get('email', null);

        if ($token !== null && $email !== null) {
            $account = $this->pixie->orm->get('account')->where('email', $email)->find();
            if ($account->loaded()) {
                $pwTokenEntry = $this->pixie->orm->get('pwresettoken')->where('token', $token)->where('and', array('accountID', $account->accountID))->find();
                if ($pwTokenEntry->loaded() && $pwTokenEntry->valid == 1) {
                    $this->setSubview("passwordreset2");
                    $this->view->message = "Geben Sie Ihr neues Kennwort ein und bestätigen Sie es nochmal!";
                    $this->view->query = "?token=$token&email=$email";
                    return;
                }
            }
        } else {
        }
        $this->view->showMessage = false;
        $this->setSubview('passwordreset');
    }

    /**
     *
     */
    public function action_logout()
    {
        $this->pixie->auth->logout();
        $this->redirect('/login');
    }

    /**
     * @param String $email
     * @return \App\Model\Account | null
     */
    private function getAccount($email)
    {
        $account = $this->pixie->orm->get('account')->where('email', $email)->find();
        if ($account->loaded())
            return $account;
        return null;
    }

    /**
     * @param int $accountID
     */
    private function invalidateOldResetToken($accountID)
    {
        /**
         * @var \App\Model\Pwresettoken[] $oldPwResetToken
         */
        $oldPwResetToken = $this->pixie->orm->get('pwresettoken')->where('accountID', $accountID)->find_all();
        foreach ($oldPwResetToken as $token) {
            $token->valid = 0;
            $token->save();
        }
    }

    /**
     * @param \App\Model\Account $account
     * @param string $token
     */
    private function sendResetTokenEmail($account, $token)
    {
        /**
         * @var \PHPixie\Email $emailModul
         */
        $emailModul = $this->pixie->email;
        $emailBody = $this->pixie->haml->get('Email/resettoken');
        $emailBody->account = $account;
        $emailBody->resetLink = "/auth/passwordreset/?token=" . $token . "&email=" . $account->email;
        //TODO Email sending
        //echo $this->pixie->config->get('app.base_url') . $emailBody->resetLink;
        //$emailModul->send($account->email, 'web@master-de', 'Liederkranz Passwort Reset', "");
    }

    /**
     * @param int $accountID
     * @return \App\Model\Pwresettoken
     */
    private function generateResetToken($accountID)
    {
        $pwResetToken = $this->pixie->orm->get('pwresettoken');
        $pwResetToken->token = rtrim(base64_encode(md5(microtime())), "=");
        $pwResetToken->accountID = $accountID;
        return $pwResetToken;
    }

    /**
     * @param int $accountID
     * @param \App\Pixie $pixie
     * @param string $password
     */
    public static function updatePassword($password, $pixie, $accountID = -1)
    {
        /**
         * @var \PHPixie\Auth\Service $auth
         * @var \App\Model\Account $account
         */
        $auth = $pixie->auth;

        if ($accountID > 0) {
            $account = $pixie->orm->get('account')->where('accountID', $accountID)->find();
        } else {
            $account = $auth->user();
        }

        $account->password = $auth->provider('password')->hash_password($password);
        $account->save();
    }

}