<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 31.03.2015
 * Time: 11:18
 */

namespace App\Controller;


class Download extends \App\Page
{

    public function before()
    {
        parent::before();
    }

    public function action_index(){
        $this->redirect('/download/list');
    }

    public function action_list()
    {
        $current_page = $this->request->param('page');
        $items = $this->pixie->orm->get('downloadgroup');
        $pager = $this->pixie->paginate->orm($items, $current_page, 10);
        $pager->set_url_route('list');
        $this->view->pager_url = function($page){return '/download/list/page-'.$page;};
        $this->view->page = $current_page;
        $this->view->pagecount = $pager->num_pages;;
        $this->setSubview('downloadlist');
        $this->view->downloadlist = $pager->current_items();
    }

}