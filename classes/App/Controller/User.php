<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 21.07.2015
 * Time: 22:50
 */

namespace App\Controller;


use App\Page;

class User extends Page
{


    public function action_index(){
        $this->redirect('/user/show');
    }

    public function action_show(){
        $user = $this->pixie->auth->user();

        $this->setSubview('profil');
        $this->view->user = $user;
    }

    public function action_edit(){

    }

}