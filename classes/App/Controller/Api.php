<?php
/**
 * Created by PhpStorm.
 * User: Jan Koschke
 * Date: 20.07.2015
 * Time: 19:29
 */

namespace App\Controller;


use App\Controller\Api\AbstractApiController;
use App\Controller\Api\AbstractApiMethod;
use App\Exception\ApiException;
use App\Model\ApiResponse;
use App\Page;
use MtHaml\Exception;

class Api extends Page
{
    /**
     * @var ApiResponse
     */
    private $result = null;

    /**
     * @var AbstractApiMethod
     */
    private $apiController = null;

    /**
     * Vorbereitungen für API mit JSON-Result
     */
    public function before()
    {
        $this->auth = $this->pixie->auth->service('std');
        $this->view = $this->pixie->haml->get('api');
        if ($this->pixie->auth->user() != null) {
            $this->view->user = $this->pixie->auth->user();
        } else {
            $module = ucfirst($this->request->param('module'));
            if ($module !== "Auth") {
                $this->response->add_header('HTTP/1.1 401 Unauthorized');
                $this->result = new ApiResponse(array(
                    'reason' => 'Unauthorized',
                    'code' => 401
                ), 401);
                $this->after();
            }
        }
    }

    public
    function action_run()
    {
        $method = 'action_' . strtolower($this->request->method);
        $action = ucfirst($this->request->param('apimethod'));
        $module = ucfirst($this->request->param('module'));

        /**
         * @var String $class
         */
        $class = 'App\\Controller\\Api\\' . $module . '\\' . $action;

        try {
            if (!class_exists($class)) {
                throw new ApiException('Class does not exist!', 401);
            }

            /**
             * @var AbstractApiMethod $apiMethod
             */
            $apiMethod = new $class($this->pixie, $this->request);
            if (!method_exists($apiMethod, $method))
                throw new \BadMethodCallException('REST-Method not supported!', 404);

            $this->result = $apiMethod->$method();
        } catch (\Exception $e) {
            $this->result = new ApiResponse(array(
                'reason' => $e->getMessage(),
                'code' => $e->getCode()
            ), $e->getCode());
        }
    }

    /**
     * Render View mit JSON Result und setze Header
     */
    public
    function after()
    {
        $this->view->result = $this->result->getResults();

        $this->response->add_header('Content-Type: application/json');
        http_response_code($this->result->getCode());
        $this->response->body = $this->view->render();
        $this->execute = false;
    }

}