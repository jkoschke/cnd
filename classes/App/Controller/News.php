<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 29.03.2015
 * Time: 13:13
 */

namespace App\Controller;


use PHPixie\Exception\PageNotFound;

class News extends \App\Page
{

    public function action_index(){
        $this->redirect('/news/list');
    }


    /* Only from POST */
    public function action_comment()
    {
        if ($this->request->method != 'POST') {
            echo "hallo";
            $this->redirect('/news/list');
        }
        //Do Comment
        $this->setSubview('hello');
        $this->view->message = 'Comment saved';
    }

    public function action_list()
    {
        $current_page = $this->request->param('page');
        $items = $this->pixie->orm->get('news');
        $pager = $this->pixie->paginate->orm($items, $current_page, 10);
        $pager->set_url_route('list');
        $this->view->page = $current_page;
        $this->view->pagecount = $pager->num_pages;;
        $this->view->pager_url = function($page){return '/news/list/page-'.$page;};

        $this->setSubview('newslist');
        $this->view->newslist = $pager->current_items();
    }

    public function action_detail(){

        $this->setSubview('newsdetail');
        $newsItem = $this->pixie->orm->get('news')->where('newsID', $this->request->param('id'))->find();
        $commentlist = $this->pixie->orm->get('comment')->where('newsID', $this->request->param('id'))->find_all();
        if($newsItem->loaded()){
            $this->view->newsitem = $newsItem;
            $this->view->commentlist = $commentlist;
        }else{
            throw new PageNotFound('Die gewünschte Seite konnte nicht gefunden werden!');
        }

    }

}