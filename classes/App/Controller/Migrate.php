<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 26.05.2015
 * Time: 20:43
 */

namespace App\Controller;


use PHPixie\Exception\PageNotFound;

class Migrate extends \PHPixie\Migrate\Controller {

    public function action_index(){
        if($this->pixie->auth->user()->hasRole('role.admin')){
            parent::action_index();
        }else{
            throw new PageNotFound("You're not permitted to see this Page");
        }
    }

}