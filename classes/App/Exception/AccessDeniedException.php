<?php
/**
 * Created by PhpStorm.
 * User: aw14353
 * Date: 06.08.2015
 * Time: 07:41
 */

namespace App\Exception;


class AccessDeniedException extends \Exception
{

    public function __construct($message){
        parent::__construct($message, 403);
    }

}