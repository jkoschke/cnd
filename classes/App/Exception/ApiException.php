<?php
/**
 * Created by PhpStorm.
 * User: aw14353
 * Date: 27.07.2015
 * Time: 13:29
 */

namespace App\Exception;


class ApiException extends \Exception
{

    /**
     * @param string $message
     * @param int $code
     */
    public function __construct($message, $code=500){
        parent::__construct($message, $code);
    }

}