<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 23.07.2015
 * Time: 00:43
 */

namespace App\Model;


class Right extends \PHPixie\ORM\Model{

    public $id_field='rightID';

    public $table='rights';

    public $connection = 'default';

    protected $belongs_to = array(
        '_roleright' => array(
            'model' => 'rolerights',
            'key' => 'rightID'
        ),
        '_accountright' => array(
            'model' => 'accountrights',
            'key' => 'rightID'
        )
    );

    /**
     * @param String $rightIdent
     * @return \App\Model\Right | null
     */
    public function getRight($rightIdent){
        $right = $this->where('identifier', $rightIdent)->find();
        if($right->loaded()){
            return $right;
        }
        return null;
    }


}