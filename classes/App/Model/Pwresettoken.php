<?php
/**
 * Created by PhpStorm.
 * User: aw14353
 * Date: 30.07.2015
 * Time: 09:04
 */

namespace App\Model;


use PHPixie\ORM\Model;

class Pwresettoken extends Model
{

    public $id_field = 'resettokenID';

    public $table = 'account_passwort_reset_tokens';

    public $connection = 'default';

    protected $belongs_to = array(
        '_account' => array(
            'model' => 'account',
            'key' => 'accountID'
        )
    );

    public function getAccount(){
        return $this->_account;
    }

}