<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 30.05.2015
 * Time: 17:04
 */

namespace App\Model;


use PHPixie\ORM\Model;


class Comment extends Model
{

    public $id_field = 'newsCommentID';

    public $table = 'news_comments';

    public $connection = 'default';

    protected $has_one = array(
        '_author' => array(
            'model' => 'account',
            'key' => 'accountID'
        )
    );

    public function getAuthor()
    {
        return $this->_author->where('accountID', $this->author)->find();
    }

}