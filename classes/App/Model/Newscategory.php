<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 31.05.2015
 * Time: 17:02
 */

namespace App\Model;


use PHPixie\ORM\Model;

class Newscategory extends Model{

    public $id_field='newsCategoryID';

    public $table='news_categories';

    public $connection = 'default';


}