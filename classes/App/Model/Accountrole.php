<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 23.05.2015
 * Time: 04:05
 */

namespace App\Model;


class Accountrole extends \PHPixie\ORM\Model
{
    public $id_field = 'roleID';

    public $table = 'account_roles';

    public $connection = 'default';

    protected $belongs_to = array(
        '_account' => array(
            'model' => 'account',
            'key' => 'accountID'
        ),
        '_role' => array(
            'model' => 'role',
            'key' => 'roleID'
        )
    );

    /**
     * @return \App\Model\Role
     */
    public function getRole(){
        return $this->_role->where('roleID', $this->roleID)->find();
    }

    /**
     * @return \App\Model\Account
     */
    public function getUser(){
        //return $this->account->find();
        return $this->_account->where('accountID', $this->accountID)->find();
    }
}