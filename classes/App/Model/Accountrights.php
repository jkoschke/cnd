<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 23.05.2015
 * Time: 02:42
 */

namespace App\Model;


class Accountrights extends \PHPixie\ORM\Model{

    public $id_field='rightID';

    public $table='rights';

    public $connection = 'default';

    protected $has_many=array(
        'roles'=>array(
            'model'=>'rolerights',
            'key'=>'rightID'
        )
    );

}