<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 09.06.2015
 * Time: 20:32
 */

namespace App\Model;


class Filecategory extends \PHPixie\ORM\Model
{

    public $id_field = 'categoryID';

    public $table = 'download_file_categories';

    public $connection = 'default';

    protected $belongs_to = array(
        '_file' => array(
            'model' => 'file',
            'key' => 'category'
        )
    );

    public function getFiles()
    {
        return $this->_file->find_all();
    }

}