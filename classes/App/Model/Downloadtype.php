<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 09.06.2015
 * Time: 20:06
 */

namespace App\Model;


class Downloadtype extends \PHPixie\ORM\Model{

    public $id_field='typeID';

    public $table='download_type';

    public $connection = 'default';

    protected $belongs_to=array(
        '_group'=>array(
            'model'=>'downloadgroup',
            'key'=>'type'
        )
    );

}