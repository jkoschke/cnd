<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 25.03.2015
 * Time: 10:47
 */

namespace App\Model;


class Account extends \PHPixie\ORM\Model
{
    public $id_field = 'accountID';

    public $table = 'accounts';

    public $connection = 'default';

    protected $belongs_to = array(
        '_accountroles' => array(
            'model' => 'accountrole',
            'key' => 'accountID'
        )
    );
    /**
     * @var \App\Model\Right[] $rights
     */
    private $rights;

    /**
     * @var \App\Model\Role[] $roles
     */
    private $roles;



    //<editor-fold desc="Getter">
    /**
     * @param String $rightIdent
     * @return bool
     */
    public function hasRight($rightIdent)
    {
        if (!isset($this->rights))
            $this->rights = $this->getUserRights();
        if (isset($this->rights[$rightIdent]) && $this->rights[$rightIdent]->identifier === $rightIdent) {
            return true;
        }
        return false;
    }

    /**
     * @param String $roleIdent Rollenname
     * @return bool
     */
    public function hasRole($roleIdent)
    {
        if (!isset($this->roles))
            $this->roles = $this->getUserRoles();
        if (isset($this->roles[$roleIdent]) && $this->roles[$roleIdent]->identifier === $roleIdent) {
            return true;
        }
        return false;
    }

    /**
     * @return \App\Model\Role[]
     */
    public function getUserRoles()
    {
        /**
         * @var \App\Model\Accountrole[] $accountRoles
         */
        $accountRoles = $this->_accountroles->where('accountID', $this->accountID)->find_all();
        $roles = array();
        foreach ($accountRoles as $accountRole) {
            $role = $accountRole->getRole();
            $roles[$role->identifier] = $role;
        }

        return $roles;
    }


    /**
     * @return \App\Model\Right[]
     */
    private function getUserRights()
    {
        if (!isset($this->roles))
            $this->roles = $this->getUserRoles();
        $accountRoles = $this->roles;
        $rights = array();
        foreach ($accountRoles as $role) {
            foreach ($role->getRights() as $right) {
                $rights[$right->identifier] = $right;
            }
        }
        return $rights;
    }
//</editor-fold>

    //<editor-fold desc="Setter">
    public function addRole($role)
    {
    }

    public function removeRole($role)
    {
    }
    //</editor-fold>
}