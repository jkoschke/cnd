<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 23.05.2015
 * Time: 02:39
 */

namespace App\Model;


class Role extends \PHPixie\ORM\Model
{

    public $id_field = 'roleID';

    public $table = 'roles';

    public $connection = 'default';

    protected $has_many = array(
        '_rights' => array(
            'model' => 'rolerights',
            'key' => 'roleID'
        )
    );

    /**
     * @return \App\Model\Rights[]
     */
    public function getRights()
    {
        /**
         * @var \App\Model\Rolerights $roleRights
         */
        $roleRights = $this->_rights->where('roleID', $this->roleID)->find_all();
        $rights = array();

        foreach ($roleRights as $roleRight) {
            $rights[] = $roleRight->getRight();
        }
        return $rights;
    }

    /**
     * @param String $rightIdent Rechteidentifier
     * @return bool
     */
    public function hasRight($rightIdent)
    {
        foreach($this->getRights() as $right){
            if($right->identifier === $rightIdent){
                return true;
            }
        }
        return false;
    }

}