<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 09.06.2015
 * Time: 20:09
 */

namespace App\Model;


class File extends \PHPixie\ORM\Model{

    public $id_field='fileID';

    public $table='download_file';

    public $connection = 'default';

    protected $has_one=array(
        '_category'=>array(
            'model'=>'filecategory',
            'key'=>'categoryID'
        ),
        '_uploader'=>array(
            'model'=>'account',
            'key'=>'categoryID'
        )
    );

    /**
     * @return String Gibt den Namen der Kategorie zurück
     */
    public function getCategory(){
        return $this->_category->where('categoryID', $this->category)->find();
    }
}