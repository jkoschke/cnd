<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 23.05.2015
 * Time: 03:36
 */

namespace App\Model;


class Rolerights extends \PHPixie\ORM\Model
{

    public $id_field = 'rrID';

    public $table = 'role_rights';

    public $connection = 'default';

    protected $has_one = array(
        'right' => array(
            'model' => 'right',
            'key' => 'rightID'
        ),
        'role' => array(
            'model' => 'role',
            'key' => 'roleID'
        )
    );


    /**
     * @return \App\Model\Role
     */
    public function getRole()
    {
        return $this->role->where('roleID', $this->roleID)->find();
    }

    /**
     * @return \App\Model\Right
     */
    public function getRight()
    {
        return $this->right->where('rightID', $this->rightID)->find();
    }

}