<?php
/**
 * Created by PhpStorm.
 * User: aw14353
 * Date: 06.08.2015
 * Time: 08:13
 */

namespace App\Model;


class ApiResponse
{
    /**
     * @var int
     */
    private $statusCode = 200;

    /**
     * @var array
     */
    private $results = array();


    /**
     * @param array $result
     * @param int $code
     */
    public function __construct($result, $code = 200)
    {
        $this->statusCode = $code;
        $this->results = $result;
    }

    /**
     * @return array
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param String $key
     * @return String | array
     */
    public function getResult($key)
    {
        return $this->results[$key];
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @param array $results
     */
    public function setResult($results)
    {
        $this->results = $results;
    }


}