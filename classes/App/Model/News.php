<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 31.03.2015
 * Time: 14:29
 */

namespace App\Model;


class News extends \PHPixie\ORM\Model {

    public $id_field='newsID';

    public $table='news';

    public $connection = 'default';

    protected $has_one = array(
        '_author'=>array(
            'model'=>'account',
            'key'=>'accountID'
        ),
        '_editor'=>array(
            'model'=>'account',
            'key'=>'accountID'
        ),
        '_category'=>array(
            'model'=>'newscategory',
            'key'=>'newsCategoryID'
        )
    );

    public function getAuthor(){
        return $this->_author->where('accountID', $this->author)->find();
    }

    public function getCategory(){
        return $this->_category->where('newsCategoryID', $this->category)->find();
    }

    public function getEditor(){
        return $this->_editor->where('accountID', $this->updater)->find();
    }

    public function hasEditor(){
        if($this->updater !== null){
            return true;
        }
        return false;
    }

}