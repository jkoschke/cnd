<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 09.06.2015
 * Time: 20:02
 */
namespace App\Model;


class Downloadgroup extends \PHPixie\ORM\Model {

    public $id_field = 'groupID';

    public $table = 'download_group';

    public $connection = 'default';

    protected $has_one = array(
        '_type' => array(
            'model' => 'downloadtype',
            'key' => 'typeID'
        )
    );

    protected $has_many = array(
        '_files' => array(
            'model' => 'file',
            'key' => 'group'
        )
    );

    public function getType()
    {
        return $this->_type->where('typeID', $this->type)->find();
    }

    public function getFiles(){
        return $this->_files->find_all();
    }

}