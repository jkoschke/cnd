<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 08.07.2015
 * Time: 02:17
 */

namespace App;


class AuthPage extends \App\Page{

    public function before()
    {
        $this->view = $this->pixie->haml->get('main');
        $this->auth = $this->pixie->auth->service('std');
        $this->view->nav = false;

        if ($this->pixie->auth->user() != null) {
            $this->view->user = $this->pixie->auth->user();
        }
    }

}