<?php

namespace App;

/**
 * Base controller
 *
 * @property-read \App\Pixie $pixie Pixie dependency container
 */
class Page extends \PHPixie\Controller
{

    protected $view;
    protected $auth;

    public function before()
    {
        $this->view = $this->pixie->haml->get('main');
        $this->auth = $this->pixie->auth->service('std');
        $this->view->nav = true;


        if ($this->pixie->auth->user() != null) {
            $this->view->user = $this->pixie->auth->user();
        } else {
            $this->redirect('/auth/login/?redirect=' . parse_url($this->request->url(), PHP_URL_PATH));
        }
    }

    public function after()
    {
        $this->response->body = $this->view->render();
    }

    protected function logged_in($role = null)
    {
        if ($this->pixie->auth->user() == null) {
            $this->redirect('/login');
            return false;
        }

        if ($role && !$this->pixie->auth->has_role($role)) {
            $this->response->body = "You don't have the permissions to access this page";
            $this->execute = false;
            return false;
        }

        return true;
    }

    public function setSubview($subview){
        $this->view->subview = $subview;
    }
}
