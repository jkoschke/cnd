<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 30.05.2015
 * Time: 00:22
 */

namespace App\View;

use DateTime;

class Helper extends \PHPixie\View\Helper{

    protected $aliases = array(
        '_'  => 'output',
        //We alias the repeat method to $_r variable
        '_r' => 'repeat',
        '_nt' => 'newstext',
        '_date' => 'date',
        '_time' => 'time',
        '_cropNews' => 'cropNews'
    );
    //Lets define a new method for our helper
    public function repeat($str, $times) {
        for($i = 0;$i<$times; $i++)
            echo $str;
    }

    public function newstext($str){
        echo $str;
    }

    public function date($timestamp){
        /**
         * @param DateTime $datetime DateTime
         */
        $datetime = DateTime::createFromFormat('Y-m-d H:i:s', $timestamp);
        echo date('d.m.Y', $datetime->getTimestamp());
    }

    public function time($timestamp){
        /**
         * @param DateTime $datetime DateTime
         */
        $datetime = DateTime::createFromFormat('Y-m-d H:i:s', $timestamp);
        echo date('H:i:s', $datetime->getTimestamp());
    }

    public function cropNews($text, $newsID, $length=700){
        $link = "/news/detail/$newsID";
        if(strlen($text) > $length){
            $cropped = substr($text, 0, $length);
            $final = trim($cropped)."... <a href='$link'>Weiter lesen!</a>";
            return $final;
        }else{
            return $text."<br><a href='$link'>Zeige Detailansicht</a>";
        }
    }
}