<?php

namespace App;

use App\View\Helper;

/**
 * Pixie dependency container
 *
 * @property-read \PHPixie\DB $db Database module
 * @property-read \PHPixie\ORM $orm ORM module
 */
class Pixie extends \PHPixie\Pixie
{

    protected $modules = array(
        'db' => '\PHPixie\DB',
        'orm' => '\PHPixie\ORM',
        'auth' => '\PHPixie\Auth',
        'paginate' => '\PHPixie\Paginate',
        'email' => '\PHPixie\Email',
        'validate' => '\PHPixie\Validate',
        'haml' => '\PHPixie\Haml',
        'migrate' => '\PHPixie\Migrate'
    );

    protected function after_bootstrap()
    {
        // Whatever code you want to run after bootstrap is done.
        if ($this->config->get('app.debugging')) {

        } else {
            $this->debug->display_errors = false;
        }


    }

    public function view_helper()
    {
        return new Helper($this);
    }

}
