<?php

return array(
    'login' => array(
        '/login',
        array(
            'controller' => 'auth',
            'action' => 'login'
        ),
    ),
    'newscomment' => array(
        '/comment',
        array(
            'controller' => 'news',
            'action' => 'comment'
        ), 'POST'
    ),
    'logout' => array(
        '/logout',
        array(
            'controller' => 'auth',
            'action' => 'logout'
        )
    ),
    'list' => array(
        '/<controller>/list(/page-<page>)',
        array(
            'page' => 1,
            'action' => 'list'
        )
    ),
    'api' => array(
      '/api/<module>/<apimethod>(/<id>(/<subaction>))',
        array(
            'controller' => 'api',
            'action' => 'run'
        )
    ),
    'default' => array(
        '(/<controller>(/<action>(/<id>)))',
        array(
            'controller' => 'news',
            'action' => 'index'
        ),
    ),
);
