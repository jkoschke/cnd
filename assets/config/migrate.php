<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 26.05.2015
 * Time: 20:35
 */
return array (

    //As with the datatabase the migration module can handle multiple
    //configurations. 'Default' is the default one
    'default' =>
        array (

            //Specify a database connection to use
            'connection' => 'default',

            //Path to a folder where migration files for this
            //configuration are stored
            'path' => '/assets/migrations/',

            //Name of the last migration applied to the database,
            //it will be automatically updated when you migrate
            'current_version' => null
        ),
);