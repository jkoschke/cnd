<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 26.05.2015
 * Time: 22:44
 */


return array(

    'accounts' => array(
        'email' => array(
        )
    ),

    'account_passwort_reset_tokens' => array(
        'accountID' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'int',
            'size' => 10
        )
    ),

    'account_register_token' => array(
        'accountID' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'int',
            'size' => 10
        )
    ),

    'account_roles' => array(
        'accountID' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'int',
            'size' => 10
        ),
        'roleID' => array(
            'type' => 'int',
            'size' => 10
        )
    ),

    'news' => array(
        'category' => array(
            'type' => 'int',
            'size' => 11
        ),
        'author' => array(
            'type' => 'int',
            'size' => 10
        ),
        'updater' => array(
            'type' => 'int',
            'size' => 10,
            'default' => 'NULL'
        )
    ),

    'news_comments' => array(
        'author' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'int',
            'size' => 10
        )
    ),

    'rights' => array(
        'identifier' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'varchar',
            'size' => 100
        )
    ),

    'roles' => array(
        'identifier' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'varchar',
            'size' => 100
        )
    ),

    'roles_rights' => array(
        'roleID' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'int',
            'size' => 10
        ),
        'rightID' => array(
            'type' => 'int',
            'size' => 10
        )
    )
);