<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 26.05.2015
 * Time: 20:36
 */

return array(

    'accounts' => array(
        'accountID' => array(

            //'id' type is a shorthand for
            // INT AUTO_INCREMENT PRIMARY_KEY
            'type' => 'id',
            'size' => 10
        ),
        'name' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'varchar',
            'size' => 100,
            'default' => '\'-\''
        ),
        'surname' => array(
            'type' => 'varchar',
            'size' => 100,
            'default' => '\'-\''
        ),
        'email' => array(
            'type' => 'varchar',
            'size' => 100,
            'default' => '\'-\''
        ),
        'password' => array(
            'type' => 'varchar',
            'size' => 255,
            'default' => '\'-\''
        ),
        'created_at' => array(
            'type' => 'timestamp',
            'default' => 'CURRENT_TIMESTAMP'
        ),
        'updated_at' => array(
            'type' => 'timestamp',
        ),
        'deleted' => array(
            'type' => 'int',
            'size' => 1,
            'default' => '0'
        )
    ),

    'account_passwort_reset_tokens' => array(
        'resettokenID' => array(

            //'id' type is a shorthand for
            // INT AUTO_INCREMENT PRIMARY_KEY
            'type' => 'id',
            'size' => 20
        ),
        'accountID' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'int',
            'size' => 10
        ),
        'token' => array(
            'type' => 'varchar',
            'size' => 50
        ),
        'created_at' => array(
            'type' => 'timestamp',
            'default' => 'CURRENT_TIMESTAMP'
        ),
        'updated_at' => array(
            'type' => 'timestamp',
        ),
        'valid' => array(
            'type' => 'int',
            'size' => 1,
            'default' => '0'
        )
    ),

    'account_register_token' => array(
        'registertokenID' => array(

            //'id' type is a shorthand for
            // INT AUTO_INCREMENT PRIMARY_KEY
            'type' => 'id',
            'size' => 20
        ),
        'accountID' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'int',
            'size' => 10
        ),
        'token' => array(
            'type' => 'varchar',
            'size' => 50
        ),
        'created_at' => array(
            'type' => 'timestamp',
            'default' => 'CURRENT_TIMESTAMP'
        ),
        'updated_at' => array(
            'type' => 'timestamp',
        ),
        'valid' => array(
            'type' => 'int',
            'size' => 1,
            'default' => '0'
        )
    ),

    'account_roles' => array(
        'accountroleID' => array(

            //'id' type is a shorthand for
            // INT AUTO_INCREMENT PRIMARY_KEY
            'type' => 'id',
            'size' => 10
        ),
        'accountID' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'int',
            'size' => 10
        ),
        'roleID' => array(
            'type' => 'int',
            'size' => 10
        ),
    ),

    'news' => array(
        'newsID' => array(

            //'id' type is a shorthand for
            // INT AUTO_INCREMENT PRIMARY_KEY
            'type' => 'id',
            'size' => 10
        ),
        'title' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'varchar',
            'size' => 100
        ),
        'text' => array(
            'type' => 'varchar',
            'size' => 11200
        ),
        'category' => array(
            'type' => 'int',
            'size' => 11
        ),
        'author' => array(
            'type' => 'int',
            'size' => 10
        ),
        'created_at' => array(
            'type' => 'timestamp',
            'default' => 'CURRENT_TIMESTAMP'
        ),
        'updated_at' => array(
            'type' => 'timestamp',
            'default' => 'NULL'
        ),
        'updater' => array(
            'type' => 'int',
            'size' => 10,
            'default' => 'NULL'
        )
    ),

    'news_categories' => array(
        'newsCategoryID' => array(

            //'id' type is a shorthand for
            // INT AUTO_INCREMENT PRIMARY_KEY
            'type' => 'id',
            'size' => 10
        ),
        'name' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'varchar',
            'size' => 50
        )
    ),

    'news_comments' => array(
        'newsCommentID' => array(

            //'id' type is a shorthand for
            // INT AUTO_INCREMENT PRIMARY_KEY
            'type' => 'id',
            'size' => 10
        ),
        'author' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'int',
            'size' => 10
        ),
        'text' => array(
            'type' => 'varchar',
            'size' => 3000,
            'default' => ''
        ),
        'created_at' => array(
            'type' => 'timestamp',
            'default' => 'CURRENT_TIMESTAMP'
        )
    ),

    'rights' => array(
        'rightID' => array(

            //'id' type is a shorthand for
            // INT AUTO_INCREMENT PRIMARY_KEY
            'type' => 'id',
            'size' => 10
        ),
        'identifier' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'varchar',
            'size' => 100
        ),
        'rightName' => array(
            'type' => 'varchar',
            'size' => 100
        )
    ),

    'roles' => array(
        'roleID' => array(

            //'id' type is a shorthand for
            // INT AUTO_INCREMENT PRIMARY_KEY
            'type' => 'id',
            'size' => 10
        ),
        'identifier' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'varchar',
            'size' => 100
        ),
        'roleName' => array(
            'type' => 'varchar',
            'size' => 100
        )
    ),

    'roles_rights' => array(
        'rrID' => array(

            //'id' type is a shorthand for
            // INT AUTO_INCREMENT PRIMARY_KEY
            'type' => 'id',
            'size' => 10
        ),
        'roleID' => array(
            //'Name' will be a column of type VARCHAR(255)
            'type' => 'int',
            'size' => 10
        ),
        'rightID' => array(
            'type' => 'int',
            'size' => 10
        )
    )
);