-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               5.6.21 - MySQL Community Server (GPL)
-- Server Betriebssystem:        Win32
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportiere Datenbank Struktur für chor
DROP DATABASE IF EXISTS `chor`;
CREATE DATABASE IF NOT EXISTS `chor` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `chor`;


-- Exportiere Struktur von Tabelle chor.accounts
CREATE TABLE IF NOT EXISTS `accounts` (
  `accountID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '-',
  `surname` varchar(100) NOT NULL DEFAULT '-',
  `email` varchar(100) NOT NULL DEFAULT '-',
  `password` varchar(100) NOT NULL DEFAULT '-',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`accountID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt
INSERT INTO `accounts` (`accountID`, `name`, `surname`, `email`, `password`, `updated_at`, `created_at`, `deleted`) VALUES
(1,	'Koschke',	'Jan',	'jankoschke@googlemail.com',	'9f18bd7d7cd0eb47212c6995ae0eebbc:59117485555187ecd9dcb8',	'2015-02-24 12:59:43',	'2015-02-24 12:59:43',	0),
(2,	'Strobel',	'Simon',	'strobesim@gmail.com',	'08ac5ee92b61607fb04f28f4db299961:123163726455187eff9a8b9',	'2015-02-24 18:30:35',	'2015-02-24 18:30:35',	0),
(3,	'Dangel',	'Felix',	'felix.dangel@online.de',	'08ac5ee92b61607fb04f28f4db299961:123163726455187eff9a8b9',	'2015-07-21 20:34:33',	'2015-07-21 20:34:34',	0),
(4,	'Lohrer',	'Viktoria',	'viktoria180@googlemail.com',	'08ac5ee92b61607fb04f28f4db299961:123163726455187eff9a8b9',	'2015-07-21 20:43:09',	'2015-07-21 20:43:10',	0),
(5,	'Grothues',	'Felix',	'felix.grothues@gmail.com',	'08ac5ee92b61607fb04f28f4db299961:123163726455187eff9a8b9',	'2015-07-23 05:32:51',	'2015-07-23 05:32:52',	0);

-- Exportiere Struktur von Tabelle chor.account_passwort_reset_tokens
CREATE TABLE IF NOT EXISTS `account_passwort_reset_tokens` (
  `resettokenID` int(20) NOT NULL AUTO_INCREMENT,
  `accountID` int(10) NOT NULL,
  `token` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`resettokenID`),
  KEY `accountID` (`accountID`),
  CONSTRAINT `FK_account_passwort_reset_tokens_account` FOREIGN KEY (`accountID`) REFERENCES `accounts` (`accountID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt


-- Exportiere Struktur von Tabelle chor.account_register_token
CREATE TABLE IF NOT EXISTS `account_register_token` (
  `registertokenID` int(10) NOT NULL AUTO_INCREMENT,
  `accountID` int(10) NOT NULL,
  `token` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`registertokenID`),
  KEY `accountID` (`accountID`),
  CONSTRAINT `FK_account_register_token_account` FOREIGN KEY (`accountID`) REFERENCES `accounts` (`accountID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt


-- Exportiere Struktur von Tabelle chor.account_roles
CREATE TABLE IF NOT EXISTS `account_roles` (
  `accountroleID` int(10) NOT NULL AUTO_INCREMENT,
  `accountID` int(10) NOT NULL,
  `roleID` int(10) NOT NULL,
  PRIMARY KEY (`accountroleID`),
  KEY `accountID` (`accountID`),
  KEY `roleID` (`roleID`),
  CONSTRAINT `FK__account` FOREIGN KEY (`accountID`) REFERENCES `accounts` (`accountID`),
  CONSTRAINT `FK__roles` FOREIGN KEY (`roleID`) REFERENCES `roles` (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Speichert die Rolle(n) eines Benutzers.';

INSERT INTO `account_roles` (`accountroleID`, `accountID`, `roleID`) VALUES
(1,	1,	5),
(2,	2,	5),
(3,	1,	1);

-- Exportiere Struktur von Tabelle chor.download_file
CREATE TABLE IF NOT EXISTS `download_file` (
  `fileID` int(11) NOT NULL AUTO_INCREMENT,
  `filehash` varchar(100) DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `group` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `uploaded_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `uploader` int(11) DEFAULT NULL,
  PRIMARY KEY (`fileID`),
  UNIQUE KEY `filehash` (`filehash`),
  KEY `category` (`category`),
  KEY `uploader` (`uploader`),
  KEY `group` (`group`),
  CONSTRAINT `FK_download_file_accounts` FOREIGN KEY (`uploader`) REFERENCES `accounts` (`accountID`),
  CONSTRAINT `FK_download_file_download_file_categories` FOREIGN KEY (`category`) REFERENCES `download_file_categories` (`categoryID`),
  CONSTRAINT `FK_download_file_download_group` FOREIGN KEY (`group`) REFERENCES `download_group` (`groupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt
INSERT INTO `download_file` (`fileID`, `filehash`, `filename`, `group`, `category`, `uploaded_at`, `uploader`) VALUES
(1,	'123',	'Lied1 Text',	0,	1,	'2015-06-09 19:26:07',	1),
(2,	'1234',	'Lied1 Audio',	0,	2,	'2015-06-09 17:26:23',	2);

-- Exportiere Struktur von Tabelle chor.download_file_categories
CREATE TABLE IF NOT EXISTS `download_file_categories` (
  `categoryID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`categoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt
INSERT INTO `download_file_categories` (`categoryID`, `name`) VALUES
(1,	'Liedtext'),
(2,	'Audio'),
(3,	'Noten');

-- Exportiere Struktur von Tabelle chor.download_group
CREATE TABLE IF NOT EXISTS `download_group` (
  `groupID` int(11) NOT NULL,
  `name` varchar(100) DEFAULT '-',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`groupID`),
  UNIQUE KEY `name` (`name`),
  KEY `dl_type` (`type`),
  CONSTRAINT `FK_download_group_download_type` FOREIGN KEY (`type`) REFERENCES `download_type` (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt

INSERT INTO `download_group` (`groupID`, `name`, `created_at`, `type`) VALUES
(0,	'TestLied',	NULL,	1);

-- Exportiere Struktur von Tabelle chor.download_type
CREATE TABLE IF NOT EXISTS `download_type` (
  `typeID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`typeID`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt
INSERT INTO `download_type` (`typeID`, `name`) VALUES
(1,	'Lieder'),
(2,	'Sonstiges');

-- Exportiere Struktur von Tabelle chor.news
CREATE TABLE IF NOT EXISTS `news` (
  `newsID` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT 'Kein Titel',
  `text` varchar(11200) NOT NULL DEFAULT 'Kein Text',
  `category` int(11) DEFAULT NULL,
  `author` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updater` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `archived` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`newsID`),
  KEY `author` (`author`),
  KEY `updater` (`updater`),
  KEY `category` (`category`),
  CONSTRAINT `FK_news_accounts` FOREIGN KEY (`author`) REFERENCES `accounts` (`accountID`),
  CONSTRAINT `FK_news_accounts_2` FOREIGN KEY (`updater`) REFERENCES `accounts` (`accountID`),
  CONSTRAINT `FK_news_news_categories` FOREIGN KEY (`category`) REFERENCES `news_categories` (`newsCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt
INSERT INTO `news` (`newsID`, `title`, `text`, `category`, `author`, `created_at`, `updater`, `updated_at`, `archived`) VALUES
(1,	'Lorem Ipsum',	'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris.Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit.Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna. In hac habitasse platea dictumst. Curabitur at lacus ac velit ornare lobortis. Curabitur a felis in nunc fringilla tristique. Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Nunc nulla. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Donec venenatis vulputate lorem. Morbi nec metus. Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem.Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. In auctor lobortis lacus. Quisque libero metus, condimentum nec, tempor a, commodo mollis, magna. Vestibulum ullamcorper mauris at ligula. Fusce fermentum. Nullam cursus lacinia erat. Praesent blandit laoreet nibh. Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. Praesent egestas neque eu enim. In hac habitasse platea dictumst. Fusce a quam. Etiam ut purus mattis mauris sodales aliquam. Curabitur nisi. Quisque malesuada placerat nisl. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero.',	1,	1,	'2015-03-31 11:32:42',	3,	'2015-07-27 08:51:52',	0),
(3,	'Neue Chorprobe',	'Neue Probe, neue Probe, neue Probe Neue Probe, neue Probe, neue Probe heute keine Probe!!!!Neue Probe, neue Probe, neue Probe',	1,	2,	'2015-04-01 09:24:27',	3,	'2015-07-27 08:51:59',	0),
(4,	'3. News',	'<p><b>Hier habe ich ein wenig was geschrieben!</b></p>\r\n<p>Testen wir unsere News! TESTE TESTE ETafklasdfklöasklöfjkasd \r\nTesten wir unsere News! TESTE TESTE ETafklasdfklöasklöfjkasd \r\nTesten wir unsere News! TESTE TESTE ETafklasdfklöasklöfjkasd</p>\r\n<p><img src=\"http://ris.fashion.telegraph.co.uk/RichImageService.svc/imagecontent/1/TMG10811028/m/Miranda_Kerr_2902539a.jpg\" /></p>',	1,	1,	'2015-07-01 14:19:49',	NULL,	NULL,	0);


-- Exportiere Struktur von Tabelle chor.news_categories
CREATE TABLE IF NOT EXISTS `news_categories` (
  `newsCategoryID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '-',
  PRIMARY KEY (`newsCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt
INSERT INTO `news_categories` (`newsCategoryID`, `name`) VALUES
(1,	'Allgemein'),
(2,	'Chorprobe');

-- Exportiere Struktur von Tabelle chor.news_comments
CREATE TABLE IF NOT EXISTS `news_comments` (
  `newsCommentID` int(11) NOT NULL AUTO_INCREMENT,
  `newsID` int(11) DEFAULT NULL,
  `author` int(11) NOT NULL,
  `text` varchar(3000) DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`newsCommentID`),
  KEY `author` (`author`),
  KEY `newsID` (`newsID`),
  CONSTRAINT `FK__accounts` FOREIGN KEY (`author`) REFERENCES `accounts` (`accountID`),
  CONSTRAINT `FK_news_comments_news` FOREIGN KEY (`newsID`) REFERENCES `news` (`newsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt
INSERT INTO `news_comments` (`newsCommentID`, `newsID`, `author`, `text`, `created_at`) VALUES
(1,	1,	1,	'This is a Comment',	'2015-07-07 15:26:51'),
(2,	1,	2,	'Aber das ist doch ein Kommentar',	'2015-07-07 15:28:11');

-- Exportiere Struktur von Tabelle chor.rights
CREATE TABLE IF NOT EXISTS `rights` (
  `rightID` int(10) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(100) NOT NULL,
  `rightName` varchar(100) NOT NULL DEFAULT '-',
  PRIMARY KEY (`rightID`),
  UNIQUE KEY `identifier` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Enthält alle in der Web-Anwendung verfügbaren Rechte. Der Name des Rechts ("Bild hochladen") steht in rightName';

-- Daten Export vom Benutzer nicht ausgewählt
INSERT INTO `rights` (`rightID`, `identifier`, `rightName`) VALUES
(1,	'user.login',	'Login'),
(2,	'news.view',	'News anzeigen'),
(3,	'news.edit',	'News bearbeiten'),
(4,	'news.create',	'News erstellen'),
(5,	'news.delete',	'News löschen'),
(6,	'downloads.view',	'Downloads anzeigen'),
(7,	'downloads.create',	'Downloads erstellen'),
(8,	'downloads.edit',	'Downloads bearbeiten'),
(9,	'downloads.delete',	'Downloads löschen'),
(10,	'news.category.create',	'Newskategorie erstellen'),
(11,	'news.category.edit',	'Newskategorie bearbeiten'),
(12,	'news.category.delete',	'Newskategorie löschen'),
(13,	'downloads.category.create',	'Downloadkategorie erstellen'),
(14,	'downloads.category.edit',	'Downloadskategorie bearbeiten'),
(15,	'downloads.category.delete',	'Downloadskategorie löschen'),
(16,	'downloads.file.create',	'File zu Download hinzufügen'),
(17,	'downloads.file.edit',	'File von Download bearbeiten'),
(18,	'downloads.file.delete',	'File von Download löschen'),
(19,	'downloads.file.category.create',	'Filekategorie erstellen'),
(20,	'downloads.file.category.edit',	'Filekategorie bearbeiten'),
(21,	'downloads.file.category.delete',	'Filekategorie löschen'),
(22,	'usermanager.user.create',	'User anlegen'),
(23,	'usermanager.user.edit',	'Fremden User bearbeiten'),
(24,	'usermanager.user.delete',	'Fremden User löschen'),
(25,	'news.comment',	'Kommentieren von News'),
(26,	'downloads.filedownload',	'Dateien herunterladen'),
(27,	'usermanager.view',	'Usermanager anzeigen');

-- Exportiere Struktur von Tabelle chor.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `roleID` int(10) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(100) NOT NULL,
  `roleName` varchar(100) NOT NULL DEFAULT '-',
  PRIMARY KEY (`roleID`),
  UNIQUE KEY `identifier` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Enthält alle in der Web-Anwendung verfügbaren Rollen ("Administrator", "Moderator", etc)';

-- Daten Export vom Benutzer nicht ausgewählt
INSERT INTO `roles` (`roleID`, `identifier`, `roleName`) VALUES
(1,	'role.admin',	'Administrator'),
(2,	'role.newsmanager',	'Redakteur'),
(3,	'role.downloadmanager',	'Uploader'),
(4,	'role.usermanager',	'Usermanager'),
(5,	'role.user',	'Mitglied'),
(6,	'role.pseudoadmin',	'Administrator');

-- Exportiere Struktur von Tabelle chor.role_rights
CREATE TABLE IF NOT EXISTS `role_rights` (
  `rrID` int(10) NOT NULL AUTO_INCREMENT,
  `roleID` int(10) NOT NULL,
  `rightID` int(10) NOT NULL,
  PRIMARY KEY (`rrID`),
  KEY `roleID` (`roleID`),
  KEY `rightID` (`rightID`),
  CONSTRAINT `FK_role_rights_rights` FOREIGN KEY (`rightID`) REFERENCES `rights` (`rightID`),
  CONSTRAINT `FK_role_rights_roles` FOREIGN KEY (`roleID`) REFERENCES `roles` (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Speichert, welche Rechte eine Rolle besitzt';

-- Daten Export vom Benutzer nicht ausgewählt
INSERT INTO `role_rights` (`rrID`, `roleID`, `rightID`) VALUES
	(1, 5, 1),
	(2, 5, 2),
	(3, 5, 6),
	(4, 5, 26),
	(5, 5, 25),
	(6, 2, 10),
	(7, 2, 11),
	(8, 2, 3),
	(9, 2, 4),
	(10, 2, 5),
	(11, 3, 13),
	(12, 3, 14),
	(13, 3, 7),
	(14, 3, 9),
	(15, 3, 8),
	(16, 3, 19),
	(17, 3, 20),
	(18, 3, 16),
	(19, 3, 18),
	(20, 3, 17),
	(24, 4, 22),
	(25, 4, 24),
	(26, 4, 23),
	(27, 1, 1),
	(28, 1, 2),
	(29, 1, 3),
	(30, 1, 4),
	(31, 1, 5),
	(32, 1, 6),
	(33, 1, 7),
	(34, 1, 8),
	(35, 1, 9),
	(36, 1, 10),
	(37, 1, 11),
	(38, 1, 12),
	(39, 1, 13),
	(40, 1, 14),
	(41, 1, 15),
	(42, 1, 16),
	(43, 1, 17),
	(44, 1, 18),
	(45, 1, 19),
	(46, 1, 20),
	(47, 1, 21),
	(48, 1, 22),
	(49, 1, 23),
	(50, 1, 24),
	(51, 1, 25),
	(52, 1, 26),
	(79, 6, 1),
	(80, 6, 2),
	(81, 6, 3),
	(82, 6, 4),
	(83, 6, 5),
	(84, 6, 6),
	(85, 6, 7),
	(86, 6, 8),
	(87, 6, 9),
	(88, 6, 10),
	(89, 6, 11),
	(90, 6, 12),
	(91, 6, 13),
	(92, 6, 14),
	(93, 6, 15),
	(94, 6, 16),
	(95, 6, 17),
	(96, 6, 18),
	(97, 6, 19),
	(98, 6, 20),
	(99, 6, 21),
	(100, 6, 22),
	(101, 6, 23),
	(102, 6, 24),
	(103, 6, 25),
	(104, 6, 26),
	(105, 1, 27),
	(106, 6, 27),
	(107, 4, 27);

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
